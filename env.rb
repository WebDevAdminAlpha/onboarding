#!/usr/bin/ruby

require 'pathname'


# purpose of this script to do some general checks for making sure
# env is read for K8 related development
# run script with
# export GITLAB_HOME=/User/celdem/gdk-ee/gitlab ; ./env.rb

class Color
  COLOR_ESCAPES = {
    red: 31,
    green: 32,
    yellow: 33,
    orange: 40,
    blue: 34,
    magenta: 35,
    cyan: 36,
    white: 37,
    default: 39
  }

  class << self
    def c(clr, text = nil)
      "\x1B[" + (COLOR_ESCAPES[clr] || 0).to_s + 'm' +
        (text ? text + "\x1B[0m" : '')
    end
  end
end

class EnvCheck
  attr_accessor :required_executables, :gdk_home_path
  def initialize
    @required_executables = %w[docker minikube kubectl gdk gitlab-runner grep]
    @gdk_home_path = ENV.fetch('GITLAB_HOME', gdk_path)
  end

  def check
    check_executables

    Dir.chdir(gdk_home_path) do
      check_registry
      check_gitlab_runner
      check_kubectl_context
      check_cilium_pods
      check_gitlab_agent

      if check_postgres_db
        check_gitlab_cluster
      end
    end
  end

  private

  def gdk_path
    if ENV['GITLAB_HOME'].nil?
      puts 'GITLAB_HOME variable is not set please set GITLAB_HOME variable or enter it:'
      path = gets.chomp

      until Pathname.new(path).exist?
        puts 'Path name you entered is not valid please enter valid pathname: i.e (/Users/<username>/gdk-ee/gitlab):'
        path = gets.chomp
      end

      path
    end
  end

  def check_executables
    required_executables.each do |e|
      if command?(e) == false
        puts Color.c(:red, "\u{1F6D1} Please install #{e} to your local machine")
        exit 1
      else
        puts Color.c(:green, "\u{1F7E2} #{e} installed")
      end
    end
  end

  def command?(name)
    `which #{name}`
     $?.success?
  end

  def check_registry
    enabled = `gdk config get registry.enabled`
    puts Color.c(:red, "\u{1F6D1} Container regisry is not enabled in gdk config") unless enabled
  end

  def check_gitlab_runner
    `gitlab-runner status`
     if $?.success?
      puts Color.c(:green, "\u{1F7E2} gitlab runner verified")
     else
      puts Color.c(:red, "\u{1F6D1} problem detected in gitlab runner")
     end
  end

  def check_kubectl_context
    current_context = `kubectl config current-context`
    puts "\u{26A0} Current context is set to #{current_context}" unless current_context.chomp == 'minikube'
  end

  def check_cilium_pods
    output = `kubectl get pods -n gitlab-managed-apps -l k8s-app=cilium`

    if $?.success? && !output.nil?
      puts Color.c(:green, "\u{1F7E2} Found cilium pods #{output}")
    else
      puts Color.c(:red, "\u{1F6D1} Can't find cilium pods make sure you installed Cilium to your cluster \n Check https://cilium.io/ for details")
    end
  end

  def check_gitlab_agent
    enabled = `gdk config get gitlab_k8s_agent.enabled`
    puts Color.c(:orange, "\u{26A0} Gitlab Agent is disabled make sure enabling it if you are working on it") unless enabled
  end

  # checking db for rails console
  def check_postgres_db
    output = `gdk status | grep "run:.*postgres"`

    if $?.success? && !output.nil?
      puts Color.c(:green, "\u{1F7E2} Postgres is working")

      true
    else
      puts Color.c(:red, "\u{1F6D1} Postgres is not working")

      false
    end
  end

  def check_gitlab_cluster
    `rails runner "puts Environment.all.exists? && Clusters::Cluster.all.exists?" | grep 'true'`

    if $?.success?
      puts Color.c(:green, "\u{1F7E2} Rails app has cluster, check connection with K8")

      true
    else
      puts Color.c(:red, "\u{1F6D1} Rails app doesn't have any Cluster or Environment please connect K8 with it")

      false
    end
  end
end

dev_env = EnvCheck.new
dev_env.check

